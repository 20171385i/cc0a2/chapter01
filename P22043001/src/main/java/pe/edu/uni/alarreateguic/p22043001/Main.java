/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.alarreateguic.p22043001;

/**
 *
 * @author Angel Larreategui <alarreateguic@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Loop statements");
        System.out.println("For loop");
        //int i;
        for (int i = 0; i < 10; i++){
            System.out.println(i);
        }
        
        {
            int j = 0;
            System.out.println(++j);
        }
        for (int i = 9; 0 <= i; i--){
            System.out.println(i);
        }
        
        //Matriz 5x3
        int f = 5;
        int c = 3;
        for (int i = 1 ; i <= f; i++ ){
            //System.out.println("i: " + i);
            for (int j = 1; j <= c; j++){
                System.out.print("(" + i + ", " + j + ")" + '\t');
            }
            System.out.println("");
        }
        System.out.println("While loop");
        int i = 0;
        while(i < 10){
            i++;
            System.out.println(i);
        }
        i = 10;
        while(0 < i){
            System.out.println(i);
            i--;
        }
        while(i <= f){
            int j = 1;
            while(j <= c){
                System.out.print("(" + i + ", " + j + ")" + '\t');
                j++;
            }
            System.out.print("");
            i++;
        }
        i = 0;
        while(i < 0){
            System.out.println("This while message is not printed");
        }
        do {
            System.out.println("This do while message is printed");
        } while (i < 0);
        
        i = 1;
        do {
            int j = 1;
            do {
                System.out.println("(" + i + ", " + j + ")" + '\t');
                j++;
            } while(j <= c);
            System.out.println("");
            i++;
        } while(i <= f);
        
        //print primer numbers between 10 y 90
        //n -> n/1 = n  n%1 = 0, o n/n = 1  n%n = 0  1, [2, 3, ...] p
        int p = 10;
        for (int j = 2; j < p; j++){
            if (p%j == 0){
                System.out.println("This number is not prime");
                break;
            }
        }
    }
}
