/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.alarreateguic.p22042803;

/**
 *
 * @author Angel Larreategui <alarreateguic@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Conditional statements");
        /*
        int i = 29;
        if (30 < i){
            System.out.println("The number is greater to 30");
        }
        if (i % 2 == 1){
            System.out.println("The number is odd");
        }
        if (i % 5 ==0){
            System.out.println("The number is multiple of 5");
        }
        */
        /*
        int i = 30;
        if (30 < i){
            System.out.println("The number is greater to 30");
        } else
        if (i % 2 == 1){
            System.out.println("The number is odd");
        } else
        if (i % 5 ==0){
            System.out.println("The number is multiple of 5");
        } else {
            System.out.println("The number is other");
        }
            System.out.println("Here the program continue");
        */
        /*
        int i = 35;
        if (30 < i){
            System.out.println("The number is greater to 30 and the program ends");
            return;
        }
        if (i % 2 == 1){
            System.out.println("The number is odd and the program ends");
            return;
        }
        if (i % 5 ==0){
            System.out.println("The number is multiple of 5 and the program ends");
            return;
        }
        System.out.println("Here the program continue");
        */
        int i = 11;
        switch(i % 2){
            case 0:
                System.out.println("Number is even");
                break;
            case 1:
                System.out.println("Number is odd");
                break;
            default:
                System.out.println("Any other number");
        }
        System.out.println("Continue in this point");
    }
}
